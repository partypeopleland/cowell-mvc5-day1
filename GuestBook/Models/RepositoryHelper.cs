namespace GuestBook.Models
{
	public static class RepositoryHelper
	{
		public static IUnitOfWork GetUnitOfWork()
		{
			return new EFUnitOfWork();
		}		
		
		public static 留言回復Repository Get留言回復Repository()
		{
			var repository = new 留言回復Repository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static 留言回復Repository Get留言回復Repository(IUnitOfWork unitOfWork)
		{
			var repository = new 留言回復Repository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		

		public static 留言板Repository Get留言板Repository()
		{
			var repository = new 留言板Repository();
			repository.UnitOfWork = GetUnitOfWork();
			return repository;
		}

		public static 留言板Repository Get留言板Repository(IUnitOfWork unitOfWork)
		{
			var repository = new 留言板Repository();
			repository.UnitOfWork = unitOfWork;
			return repository;
		}		
	}
}