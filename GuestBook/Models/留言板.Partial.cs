namespace GuestBook.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    [MetadataType(typeof(留言板MetaData))]
    public partial class 留言板
    {
    }
    
    public partial class 留言板MetaData
    {
        [Required]
        public int id { get; set; }
        
        [StringLength(10, ErrorMessage="欄位長度不得大於 10 個字元")]
        [Required]
        public string 發言人 { get; set; }
        [Required]
        public string 留言 { get; set; }
        [Required]
        public System.DateTime 留言時間 { get; set; }
        [Required]
        public bool 已審核 { get; set; }
    
        public virtual ICollection<留言回復> 留言回復 { get; set; }
    }
}
