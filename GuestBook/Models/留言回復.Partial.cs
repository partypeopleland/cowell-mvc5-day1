namespace GuestBook.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    
    [MetadataType(typeof(留言回復MetaData))]
    public partial class 留言回復
    {
    }
    
    public partial class 留言回復MetaData
    {
        [Required]
        public int id { get; set; }
        [Required]
        public int 留言ID { get; set; }
        
        [StringLength(10, ErrorMessage="欄位長度不得大於 10 個字元")]
        [Required]
        public string 發言人 { get; set; }
        [Required]
        public string 留言 { get; set; }
        [Required]
        public System.DateTime 留言時間 { get; set; }
        [Required]
        public bool 已審核 { get; set; }
    
        public virtual 留言板 留言板 { get; set; }
    }
}
