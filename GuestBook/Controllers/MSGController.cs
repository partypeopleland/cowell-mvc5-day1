﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using GuestBook.Models;

namespace GuestBook.Controllers
{
    public class MSGController : Controller
    {
        private GBEntities db = new GBEntities();

        // GET: MSG
        public ActionResult Index()
        {
            return View(db.留言板.ToList());
        }

        // GET: MSG/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            留言板 留言板 = db.留言板.Find(id);
            if (留言板 == null)
            {
                return HttpNotFound();
            }
            return View(留言板);
        }

        // GET: MSG/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(FormCollection frm)
        {
            留言板 msg = new 留言板();
            msg.留言時間=DateTime.Now;
            msg.已審核=false;

            if (TryUpdateModel<留言板>(msg)) {
                db.留言板.Add(msg);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(frm);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            留言板 留言板 = db.留言板.Find(id);
            if (留言板 == null)
            {
                return HttpNotFound();
            }
            return View(留言板);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int ID,FormCollection frm)
        {
            var msg = db.留言板.Find(ID);

            if (TryUpdateModel<留言板>(msg,frm.AllKeys)) {
                msg.留言時間 = DateTime.Now;
                msg.已審核 = false;
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(msg);
        }

        // GET: MSG/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            留言板 留言板 = db.留言板.Find(id);
            if (留言板 == null)
            {
                return HttpNotFound();
            }
            return View(留言板);
        }

        // POST: MSG/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            留言板 留言板 = db.留言板.Find(id);
            db.留言板.Remove(留言板);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
