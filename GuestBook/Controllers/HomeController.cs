﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using GuestBook.ViewModel;

namespace GuestBook.Controllers {
    [Authorize]
    public class HomeController : Controller {
        public ActionResult Index() {
            return View();
        }

        public ActionResult About() {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact() {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [AllowAnonymous]
        public ActionResult Login(string ReturnUrl) {
            LoginVM vm = new LoginVM { ReturlUrl = ReturnUrl };
            return View(vm);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginVM vm) {
            if (!ModelState.IsValid) {
                return View(vm);
            }

            FormsAuthentication.RedirectFromLoginPage(vm.Account, false);
            return Redirect(FormsAuthentication.GetRedirectUrl(vm.Account, false));
        }

    }
}