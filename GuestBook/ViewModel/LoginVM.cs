﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GuestBook.ViewModel {
    public class LoginVM :IValidatableObject {
        public string ReturlUrl { get; set; }
        [Required]
        public string Account { get; set; }
        [Required]
        public string Password { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext) {
            if (!(Account == "mvc" && Password == "mvc")) {
                yield return new ValidationResult("無此帳號!", new string[] { "Account" });
            }
        }

    }
}